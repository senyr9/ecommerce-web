<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function bonjour($name, $last_name)
    {
        return view('home.bonjour', compact('name', 'last_name')) ;
    }

    public function salut($prenom)
    {
        return view('home.salut', compact('prenom')) ;
    }

    public function punition() 
    {
        return view('home.punition') ;
    }

    public function storePunition(Request $request)
    {
        $data = $request->all() ;

        return view("home.punition", compact('data')) ;
    }
}
