<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'created_at', 'updated_at'] ;

    /*
     * Récupérer la catégorie d'un produit
     * */
    public function category()
    {
        $this->belongsTo(Category::class) ;
    }
}
