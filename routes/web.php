<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/Bonjour/{name}/{last_name}', [HomeController::class, 'bonjour']);

Route::get('/Salut/{prenom}', [HomeController::class, 'salut']);

Route::get('/punition', [HomeController::class, 'punition']);


Route::post('/punition', [HomeController::class, 'storePunition'])->name('punition.store');



Route::resource('categories', CategoryController::class)->names('categories');