<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') | E-commerce</title>
    @section('stylesheets')
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    @show
</head>
<body>

    @include('layouts.header')

    <div class="container">
        @yield('content')
    </div>

    @include('layouts.footer')
    
    @section('javascripts')
        <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
    @show

</body>
</html>