@extends('layouts.app')

@section('title') Ajouter une nouvelle catégorie @endsection

@section('content')
    <form action="{{ route('categories.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="">Nom de ma catégorie</label>
            <input type="text" name="name" class="form-control sm-8">
        </div>
        <br>
        <input type="submit" value="Ajouter" class="btn btn-success sm-4">
        
    </form>
@endsection