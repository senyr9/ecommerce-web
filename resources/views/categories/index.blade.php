@extends('layouts.app')

@section('title') La liste des catégories @endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <br>
            <a href="{{ route('categories.create') }}" class="btn btn-success" 
            style="float: right">
                Ajouter une nouvelle catégories
            </a>
            <br>
            <table class="table table-striped">
                <thead>
                    <th>Nom</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach ($categories as $category)
                        <tr>
                            <td>{{ $category->name }}</td>
                            <td>
                                <a href="{{ route('categories.edit',$category->id) }}"
                                    class="btn btn-warning">
                                    Modifier
                                </a>
                                <a href="{{ route('categories.destroy',$category->id)}}" 
                                    class="btn btn-danger">
                                    Supprimer
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $categories->links() }}
        </div>
    </div>
@endsection