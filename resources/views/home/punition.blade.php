<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h3>Le formulaire de ma punition</h3>

    <form action="{{ route('punition.store') }}" method="post">
        @csrf
        <input type="text" name="phrase" placeholder="Ma phrase de punition"> <br>

        <select name="nbp">
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="1000">1000</option>
        </select>
        <br>
        <input type="submit" value="Générer">


        <br>

        <p>
            @isset($data)
                @for ($i=1; $i < $data['nbp'] ; $i++)
                {{ $data['phrase'] }}  <br>             
                @endfor
            @endisset
            
        </p>
    
    </form>

</body>
</html>