<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <= 20; $i++) { 
            
            if ($i<4) {
                //vêtements
                DB::table('products')->insert([
                    'name' => 'Produit '.$i,
                    'price' => random_int(1000, 50000),
                    'description' => "Ceci est la description de mon produit !",
                    'category_id' => 1
                ]);
            }elseif ($i>=4 and $i<10) {
                //chaussures
                DB::table('products')->insert([
                    'name' => 'Produit '.$i,
                    'price' => random_int(1000, 50000),
                    'description' => "Ceci est la description de mon produit !",
                    'category_id' => 2
                ]);
            }elseif ($i>=11 and $i<15) {
                // casquettes
                DB::table('products')->insert([
                    'name' => 'Produit '.$i,
                    'price' => random_int(1000, 50000),
                    'description' => "Ceci est la description de mon produit !",
                    'category_id' => 3
                ]);
            }else {
                // accessoires
                DB::table('products')->insert([
                    'name' => 'Produit '.$i,
                    'price' => random_int(1000, 50000),
                    'description' => "Ceci est la description de mon produit !",
                    'category_id' => 4
                ]);
            }

        }
    }
}
