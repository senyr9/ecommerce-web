<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Vêtement'
        ]) ;
        DB::table('categories')->insert([
            'name' => 'Chaussure'
        ]) ;
        DB::table('categories')->insert([
            'name' => 'Casquette'
        ]) ;
        DB::table('categories')->insert([
            'name' => 'Accessoires'
        ]) ;
    }
}
