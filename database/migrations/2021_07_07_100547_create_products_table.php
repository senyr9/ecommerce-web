<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name') ;
            $table->float('price')->nullable() ;
            $table->text('description')->nullable() ;
            $table->integer('stock')->default(0) ;
            $table->string('image')->nullable()
                ->default('http://accounts.warehousepulse.co.uk/56/assets/images/default_product.jp');
            $table->float('price_promo')->nullable() ;
            $table->boolean('status')->default(true) ;

            //La clé étrangère de la table Catégorie
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')
                ->on('categories');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
